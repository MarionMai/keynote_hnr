# Closing keynote Historical Network Research 2021

This projects contains the supplementary materials of the closing keynote given at the Historical Network Research Conference on the 2nd of July, 2021. It contains:
- A folder with the support of the presentation
- A folder with a selection of bibliographical references
- A folder with datasets
- A folder with figures
- A folder containing the R package "Places" (under development)
- R scripts to generate the figures and run several network analysis packages

The work presented in this communication is collaborative. Here is a non-exaustive list of people involved in alphabetical order: Anthony Andurand, Claire Bidart, Guillaume Cabanac, Denis Eckert, Michel Grossetti, Laurent Jégou, Béatrice Milard, René Sigrist, Mayline Strouk, Yann-Philippe Tastevin, Gil Viry

Two ungoing and exciting research projects: 
- NETCONF project - Networks of congresses’ participants (dir: Bastien Bernela, François Briatte & MM). 
    - NEW Website: https://netconf-geoscimo.univ-tlse2.fr 
    - Open Zotero bibliography: https://www.zotero.org/groups/2408729/netconf/library 
- [Arabesque](http://arabesque.ifsttar.fr/) (dir: Françoise Bahoken, Etienne Côme): an open and free web application to generate flow maps with many options, e.g. changing the shape of arrows 

Email: marion.maisonobe"at"cnrs.fr / Twitter: @GeoMaisonobe

Special thank you to Laurent Beauguitte, Martin Düring, Claire Lemercier & Delio Lucena-Piquero + the HNR community

![](figures/keynote_MM.png)

Musical accompaniment: [My! My! Time Flies!](https://youtu.be/OVm4diSaKw0) by Enya
