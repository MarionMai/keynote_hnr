# Script 2 by M. Maisonobe written during the preparation of the HNR closure keynote (Historical Network Analysis) - 2021-06-30
# Mapping the location of scientific stations in Arctic and their affiliation country

library(ggOceanMaps)
library(ggplot2)
library(svglite)

### Convert coordinates
require(parzer)
require(tidyverse)

coords <- read.csv("data/Research Station-Arctic.csv") %>%
          mutate(lon = parzer::parse_lon(lon), lat = parzer::parse_lat(lat)) %>%
          transform_coord(bind = T) %>%
          mutate(Station = case_when(Statuts == "locallyOwned" ~ "Domestically owned",
                                     Statuts == "foreignArcticOwned" ~ "Owned by an Arctic foreign country",
                                     Statuts == "foreignOwned" ~ "Owned by a non-Arctic foreign country",
                                     Statuts == "partiallyLocallyOwned" ~ "Co-owned by an Arctic and a non-Arctic country"))

### Create map using geom_spatial_point

plot.new()

# svglite("stations_map.svg", width = 10, height = 7)
# png("stations_map.png", width = 1000, height = 700)

install.packages("Cairo")  # One-time installation
CairoPNG("figures/stations_map_cairo.png")

basemap(limits = 50, land.col = "gray90", land.border.col = "gray90", grid.col = "gray70") +
  # annotation_scale(location = "br") +
  geom_spatial_point(data = coords, aes(x = lon, y = lat, color = Station))

dev.off()

# create a map with the quick map function

qmap(coords, color = Station)


